let timer = document.getElementById('timer');
let tglBtn = document.getElementById('toggleBtn');
let clear = document.getElementById('clear');

let watch = new Stopwatch(timer);

function start (){
    watch.start();
    tglBtn.textContent = 'Pause';
}
function stop (){
    watch.stop();
    tglBtn.textContent = 'Start';
}

tglBtn.addEventListener('click', function(){
    watch.isOn ? stop() : start();
});

clear.addEventListener('click', function(){
    watch.reset();
});